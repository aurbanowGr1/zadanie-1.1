package productsCart;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {

	public static void main(String[] args ){
		
		int a=2;
		int b = a;
		b=5;
		
		System.out.println(a+" "+ b);
		
		Product tv = new Product();
		tv.name = "telewizor";
		tv.code ="TV1234";
		tv.price = 500;
		tv.discountPrice = tv.price;

		Product tv2 = new Product("TV4321", "Samsung", 1000);
		
		tv2.price = 600;
		
		System.out.println("Nazwa: " + tv.name);
		System.out.println("kod: " + tv.code);
		System.out.println("Cena: " + tv.price);
		
		ArrayList<Product> produkty = new ArrayList<Product>();
		
		Product[] products = new Product[5];
		
		for(int i = 0; i< products.length ; i++){
			//...
			
			products[i] = new Product();
			products[i].price=200*i;
		}
		for(Product p : products){

			
			System.out.println("Nazwa: " + p.name);
			System.out.println("kod: " + p.code);
			System.out.println("Cena: " + p.price);
		}
		
	}
	
}
